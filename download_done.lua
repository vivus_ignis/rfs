local rfs = require "rfs"

rfs.dlog("download_done > init")

local _, traf_res = ngx.location.capture_multi(
   {
      { "/api/download/traffic/incr",    { args = { bytes = ngx.var.body_bytes_sent  }} },
      { "/out/api/download_stats",       { args = { host = ngx.var.host, 
                                                    did  = ngx.var.download_token, 
                                                    traffic = ngx.var.body_bytes_sent,
                                                    port = ngx.var.remote_port }} }
   }
)

rfs.dlog("download_done > download_token: " .. 
         ngx.var.download_token .. ", bytes sent: " .. tonumber(ngx.var.body_bytes_sent))

if traf_res.status ~= ngx.HTTP_OK then
   rfs.warn("download_done > error reporting download stats to remote API: " .. rfs.api_resp_log(traf_res))
end