#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"
#include <unistd.h>

/* 
 * gcc luafsmisc.c -o fsmisc.so -shared -Wall -I/usr/local/openresty/luajit/include/luajit-2.0 -fPIC
 * cp fsmisc.so /usr/local/openresty/lualib/ 
 *
 */

static int l_symlink(lua_State *L) {
  int nargs = lua_gettop(L);
  int status;

  if(nargs < 2) {
    lua_pushstring(L, "ln needs two arguments to operate");
    lua_error(L);
  }

  const char *from = luaL_checkstring(L, 1);
  const char *to   = luaL_checkstring(L, 2);
  status = symlink(from, to);
  lua_pushnumber(L, status);

  return 1;
}

static const luaL_reg fsmisc[] = {
  {"ln", l_symlink},
  {NULL, NULL}
};

int luaopen_fsmisc(lua_State *L) {
  luaL_register(L, "fsmisc", fsmisc);
  return 1;
}
