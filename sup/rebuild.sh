#!/bin/sh

ln -sf $PWD/openresty.conf /usr/local/openresty/nginx/conf/openresty.conf

ln -sf $PWD/rfs.lua /usr/local/openresty/nginx/conf/rfs.lua
ln -sf $PWD/download.lua /usr/local/openresty/nginx/conf/download.lua
ln -sf $PWD/download_done.lua /usr/local/openresty/nginx/conf/download.lua
ln -sf $PWD/upload.lua /usr/local/openresty/nginx/conf/upload.lua
ln -sf $PWD/upload_done.lua /usr/local/openresty/nginx/conf/upload_done.lua
ln -sf $PWD/feeder.lua /usr/local/openresty/nginx/conf/feeder.lua
ln -sf $PWD/files_delete.lua /usr/local/openresty/nginx/conf/files_delete.lua

./symlink-datadirs.sh
grep myself /etc/hosts >/dev/null 2>&1 || echo '127.0.0.1 myself' >> /etc/hosts
crontab -l > /tmp/crontab.root
grep auth_domains /tmp/crontab.root >/dev/null 2>&1 || \
echo '0 * * * * \'curl http://myself/api/feeder?feed=auth_domains\'' >> /tmp/crontab.root
crontab /tmp/crontab.root
