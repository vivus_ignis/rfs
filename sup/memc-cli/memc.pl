#!/usr/bin/perl

sub usage {
  print "Usage: memc.pl -s <key> <val>\n";
  print "       memc.pl -g <key>\n";
  print "       memc.pl -d <key>\n";
  exit(1);
}

&usage unless $ARGV[0];

use Memcached::Client;
my $client = Memcached::Client->new ({servers => ['127.0.0.1:11211']});
my $key;
my $val;

if ($ARGV[0] eq "-s") {
  $key=$ARGV[1] or &usage;
  $val=$ARGV[2] or &usage;
  print "$key => " if $client->set($key, $val, 0);
  print "\n";
}

if ($ARGV[0] eq "-g") {
  $key=$ARGV[1] or &usage;
  print $client->get($key);
  print "\n";
}

if ($ARGV[0] eq "-d") {
    $key=$ARGV[1] or &usage;
    print $client->delete($key);
    print "\n";
}

$client->disconnect();
