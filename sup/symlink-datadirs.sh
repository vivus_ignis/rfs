#!/bin/sh

storage_dir=/usr/local/openresty/storage

cd $storage_dir
for datadir in `mount | awk '{if($1 ~ /^\/data[0-9]+/) {print $1}}'`
  do
    ln -sf $datadir
  done
