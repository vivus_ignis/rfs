local rfs       = require "rfs"
local fs        = require "lfs"

if ngx.var.request_method ~= "POST" then
  rfs.dlog("upload_done > not a POST, lets return")
  return ngx.HTTP_OK
end

local temp_filepath = ngx.var.arg_path
local upload_token  = ngx.var["arg_X-Progress-ID"]

local file_mime     = rfs.mime:file(temp_filepath)

rfs.dlog("upload_done > init")
rfs.dlog("upload_done > file upload path = " .. ngx.var.arg_path)
rfs.dlog("upload_done > file upload hash = " .. ngx.var.arg_sha1)
rfs.dlog("upload_done > file upload size = " .. ngx.var.arg_size)
rfs.dlog("upload_done > file upload mime = " .. file_mime)

-- do we need to fetch auth_domains for the first time?
--

if ngx.var.arg_init_domains == 1 then

   local api_auth_domains_res = ngx.location.capture("/out/api/auth_domains")

   rfs.dlog("upload_done > API call: auth_domains status: " .. rfs.api_resp_log(api_auth_domains_res))

   rfs.ext_api_call_react(api_auth_domains_res, rfs.store_auth_domains)

end

-- move from /dataX/Y/<temp_filename> to /dataX/Y/<hash_dir>/<sha_filename>
--

local api_upload_finish_res = ngx.location.capture(
  "/out/api/upload_finish",
  { args = {
    upload_id = upload_token,
    hash = ngx.var.arg_sha1,
    size = ngx.var.arg_size,
    mime = file_mime
    }
  }
)

rfs.ext_api_call_react(api_upload_finish_res, function(upload_finish_resp_t)
                                            rfs.dlog("upload_done > API call: /out/api/upload_finish, ok")

                                            -- do we have orders to delete?
                                            if type(upload_finish_resp_t.data) == "table" and upload_finish_resp_t.data.action == "remove" then

                                               if os.remove(tempfile) == 0 then
                                                 rfs.dlog("upload_done > file " .. tempfile .. " removed by API request")
                                               else
                                                 rfs.dlog("upload_done > failed to remove file " .. tempfile .. " removed by API request")
                                               end

                                               return

                                            end

                                            -- settling file
                                            local _, _, dirname, _ = string.find(temp_filepath, [[^(.*)/([^/]-)$]] )
                                            local hashdir = string.sub(ngx.var.arg_sha1, 1, 1)
                                            local new_filepath = dirname .. "/" .. hashdir .. "/" .. ngx.var.arg_sha1
                                            
                                            rfs.dlog("upload_done > dirname=" .. dirname)
                                            rfs.dlog("upload_done > hashdir=" .. hashdir)
                                            rfs.dlog("upload_done > new_filepath=" .. new_filepath)

                                            local mkdir_res, mkdir_err = fs.mkdir(dirname .. "/" .. hashdir)
                                            if mkdir_res == nil then
                                               rfs.warn("upload_done > cannot create hashdir, err=" .. mkdir_err)
                                               rfs.escape(500, "upload_done > cannot create hashdir, err=" .. mkdir_err)
                                            end
                                            
                                            local mv_res, mv_err = os.rename(temp_filepath, new_filepath)
                                            if mv_res == nil then
                                               rfs.warn("upload_done > unable to rename from temporary file, err=" .. mv_err)
                                               rfs.escape(500, "upload_done", "unable to rename temporary file: " .. mv_err)
                                            end

                                            -- updating timestamp
                                            -- local api_timestamp_res = ngx.location.capture(
                                            --    "/api/download/timestamp/set",
                                            --    { args = { filepath = filepath }} )

                                            -- rfs.ext_api_call_react(api_timestamp_res, function() end)
                                            
                      end , false )