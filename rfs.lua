module("rfs", package.seeall)

local cjson = require "cjson.safe"
local magic = require "magic"

-- Data types hints:
--
-- *_t    : table
-- *_b    : binary
-- *_json : json
-- *_res  : http request result

local id_chars = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, "a", "b", "c", "d", "e", "f" }

-- function hextobin(s)
--   return(s:gsub("(..)", function(c)
--     return string.char(tonumber(c, 16))
--   end))
-- end

function get_req_id(char_tbl)
  local res = ""
  for i = 1, 16 do
    res = res .. char_tbl[math.random(1, #char_tbl)]
  end
  return res
end

function dlog(msg)
-- <<TODO>> add calling func sometime: debug.getinfo(2, "n").name
  ngx.log(ngx.DEBUG, "[" .. rfs.req_id .. "] [RF] lua > " .. msg)
end

function warn(msg)
-- <<TODO>> add calling func sometime
  ngx.log(ngx.WARN,  "[" .. rfs.req_id .. "] [RF] lua > " .. msg)
end

function escape(http_code, desc)
   ngx.header["X-Error-Description"] = desc
   ngx.header["X-Lua-Request-Id"] = rfs.req_id
   ngx.exit(http_code)
end

function api_resp_log(resp_t)
   return("status: " .. resp_t.status .. "; body: " .. string.gsub(resp_t.body, "%c", ""))
end

function ext_api_call_react(resp_t, success_cb, passthrough_b)
-- <<TODO>> check *_cb's to be functions

  passthrough_b = passthrough_b or true -- default is to passthrough

  -- External API response format
  --
  -- * successful call:
  -- { "status" : "OK", "data" : mixed }
  --
  -- * failure:
  -- { "status" : "Error", "error" : string }

  -- HTTP code

  if resp_t.status ~= ngx.HTTP_OK then
     rfs.warn("external API HTTP error: " .. rfs.api_resp_log(resp_t))
     if passthrough_b == false then
        rfs.escape(resp_t.status, "external API HTTP code: " .. resp_t.status)
     end
  end

  -- JSON validity

  rfs.dlog("ext_api_call_react > response body: '" .. resp_t.body .. "'")

  local resp_json_t, json_err = cjson.decode(resp_t.body)

  if resp_json_t == nil then
     rfs.warn("external API JSON parse error: " .. json_err)
     if passthrough_b == false then
        rfs.escape(resp_t.status, "external API JSON parse error: " .. json_err)
     end
  end

  -- JSON contents

  if resp_json_t.status ~= "OK" then
     rfs.warn("external API JSON status error: " .. resp_json_t.status)
     if passthrough_b == false then
        rfs.escape(resp_t.status, "external API JSON status error: " .. resp_json_t.status)
     end
  end

  -- JSON lookup metatable (return nil for nonexistent keys)

  local resp_json_meta_t = {}
  resp_json_meta_t.__index = function(tbl, key)
     rfs.warn("resp_json_meta_t")

     rfs.warn("resp_json_meta_t: key=" .. key)

     return rawget(tbl, key) or ""
  end

  setmetatable(resp_json_t, resp_json_meta_t)

  return success_cb(resp_json_t)

end

function store_auth_domains(domains_json)
   local api_auth_domains_save_res = ngx.location.capture(
      "/api/auth_domains/set", { args = { json = auth_domains_json }} )
   
   if api_auth_domains_save_res.status ~= 201 then
      rfs.warn("upload_done > API call: /api/auth_domains/set error: " ..
               rfs.api_resp_log(api_auth_domains_save_res))
   end
end

rfs.req_id = get_req_id(id_chars)

rfs.mime = magic.open()
rfs.mime:load("/usr/local/share/magic/magic.mgc")
rfs.mime:setflags(magic.MIME_TYPE)
