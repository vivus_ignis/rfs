--------------------------------------------------------------------------------
-- DO NOT CALL ANY EXTERNAL APIS HERE! THIS LOCATION IS PROXY_PASSED,         --
-- NO MORE PROXY_PASS ALLOWED !                                               --
--------------------------------------------------------------------------------

local rfs   = require "rfs"
local cjson = require "cjson"

rfs.dlog("upload > init ")

local vhost = ngx.var.host

rfs.dlog("upload > request_method: " .. ngx.var.request_method)

if ngx.var.request_method ~= "POST" then 
  rfs.dlog("upload > not a POST, will return")
  ngx.exit(ngx.HTTP_OK) -- stop any other processing
end

--
-- best pool
--

local api_best_pool_res = ngx.location.capture( "/api/best_pool/get" )

rfs.dlog("upload > API call: best_pool status: " .. rfs.api_resp_log(api_best_pool_res))

if api_best_pool_res.status == ngx.HTTP_OK then
   ngx.var.args = ngx.var.args .. "&zpool=" .. api_best_pool_res.body
   rfs.dlog("upload > best_pool=" .. api_best_pool_res.body)
   ngx.exit(ngx.OK) -- proceed to upload_do proxying
else
   rfs.warn("upload > API call: best_pool error: " .. rfs.api_resp_log(api_best_pool_res))
   rfs.escape(500, "upload", "int api (best_pool) http code: " .. api_best_pool_res.status)
end

--
-- auth domains
--

local api_auth_domains_res = ngx.location.capture(
   "/api/auth_domains/get", { args = { domain = vhost }}
)

rfs.dlog("upload > auth status:" .. rfs.api_resp_log(api_auth_domains_res))

if api_auth_domains_res.status == ngx.HTTP_OK then
   rfs.dlog("upload > got auth_domains result")

   local auth_domains_arr = cjson.decode(ngx.unescape_uri(api_auth_domains_res.body))

   -- search in array
   for _, v in pairs(auth_domains_arr) do
      if v == vhost then
         local auth_ok = true
         return
      end
   end

   if not auth_ok == true then
      rfs.warn("upload > domain " .. vhost .. " is not authorized to upload")
      rfs.escape(403, "upload", "domain is not authorized to upload")
   end

else
   -- no data for auth_domains, add a param for upload_done to initialize data
   ngx.var.args = ngx.var.args .. "&init_domains=1"
   rfs.warn("upload > auth_domains is empty, setting init_domains querystring param")
   return
end