local rfs   = require "rfs"
local cjson = require "cjson"

rfs.dlog("feeder > init ")

local feed = ngx.var.arg_feed

if feed == "auth_domains" then
   rfs.dlog("feeder > upload ")

   local auth_domains_res = ngx.location.capture("/out/api/auth_domains")

   rfs.dlog("feeder > auth status=" .. auth_domains_res.status .. ", body=" .. auth_domains_res.body)

   if auth_domains_res.status == ngx.HTTP_OK then
      local auth_domains_decoded = cjson.decode(auth_domains_res.body)

      if not auth_domains_decoded.status == "OK" then
         rfs.warn("feeder > auth_domains API responded with error")
         rfs.escape(500, "feeder", "ext api (auth_domains) error content")
      end

      rfs.dlog("feeder > going to store auth_domains to memcached: " .. 
               cjson.encode(auth_domains_decoded.domains))

      local auth_domains_json = cjson.encode(auth_domains_decoded.domains)
      local auth_domains_save = ngx.location.capture(
         "/api/auth_domains/set", { args = { json = auth_domains_json }}
      )

      if auth_domains_save.status ~= 201 then
         rfs.warn("feeder > cannot store auth_domains json, auth_domains_save.status=" ..
               auth_domains_save.status)
         rfs.escape(500, "feeder", "int api (auth_domains_save) error")
      end
   end

end

return