local rfs   = require "rfs"
local zpool = require "zpool"

-- get list of zpools with ONLINE status
-- get iostat for each disk in zpool, take average
-- store pool name with the lesser iostat as best_pool in memcache via API call
-- calculate overall load and store in redis via API call