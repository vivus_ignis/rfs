local rfs   = require "rfs"
local cjson = require "cjson"

rfs.dlog("download > download request")

local speed_res, threads_res = ngx.location.capture_multi(
   {
      { "/api/download/speed/get",   { args = { token = ngx.var.download_token } }},
      { "/api/download/threads/get", { args = { token = ngx.var.download_token } }}
   }
)

-- if we have speed limit set, we assume this download token is already authorized

local authorized    = 0
local threads_limit = 1

if speed_res.status == ngx.HTTP_OK then
   ngx.var.limit_rate = string.gsub(speed_res.body, "%c", "")
   rfs.dlog("download > got speed limit for download token <" .. 
            ngx.var.download_token .. ">: " .. ngx.var.limit_rate)
   rfs.dlog("download > assume this download token is already authorized")
   
   authorized = 1 
end

-- if threads count is 0, it means thread limit is exhausted

if threads_res.status == ngx.HTTP_OK then
   local threads_body = string.gsub(threads_res.body, "%c", "")
   threads_limit = tonumber(threads_body)
   rfs.dlog("download > got threads limit for download token <" .. 
            ngx.var.download_token .. ">: " .. threads_limit)

   if threads_limit < 1 or threads_limit > 16 then
      rfs.warn("download > wrong value for threads_limit, (defaulting to 1): " .. threads_limit)
      threads_limit = 1
   end

end --> threads_res.status

-- ask for authorization + get download options

if authorized ~= 1 then
   rfs.dlog("download > download request for download token <" .. 
            ngx.var.download_token .. "> is not yet authorized")
   local api_res = ngx.location.capture(
      "/out/api/download_init", { args = { did = ngx.var.download_token, host = ngx.var.host } }
   )

   if api_res.status ~= ngx.HTTP_OK then
      rfs.warn("download >  API server returned error: " .. rfs.api_resp_log(api_res))
      rfs.escape(api_res.status, "download", "ext api (download_init) http code: " .. api_res.status)
   end

   if api_res.status == ngx.HTTP_OK then
      rfs.dlog("download > got API response")

      -- parse response json
      --
      rfs.dlog("download > download options json: " .. api_res.body)
      local dload_options = cjson.decode(api_res.body)

      if not dload_options.status == "OK" then
         rfs.warn("download > API responded with error, maybe bad ticket id")
         rfs.escape(500, "download", "ext api (download_init) error content")
      end

      rfs.dlog("Download options: threads=" .. dload_options.threads .. ", speed=" .. dload_options.speed)

      -- limit_rate
      --
      ngx.var.limit_rate = dload_options.speed

      _ = ngx.location.capture(
         "/api/download/speed/set",
         { args = { token = ngx.var.download_token, speed = dload_options.speed }}
      )

      -- threads limit
      --
      threads_limit = tonumber(dload_options.threads)

      if threads_limit < 1 or threads_limit > 16 then
         rfs.warn("download > wrong value for threads_limit, (defaulting to 1): " .. threads_limit)
         threads_limit = 1
      end

      _ = ngx.location.capture(
         "/api/download/threads/set", 
         { args = { token = ngx.var.download_token, threads = dload_options.threads }}
      )
   end --> api_res.status
end --> not authorized

-- get real filepath

local filepath_res = ngx.location.capture(
   "/api/download/filepath/get", { args = { short_id = ngx.var.short_id } }
)

if filepath_res.status == ngx.HTTP_NOT_FOUND then
   rfs.warn("download > short_id not found in memcache")

   rfs.escape(404, "download", "short_id not found")
end

if filepath_res.status == ngx.HTTP_OK then
   local filepath = string.gsub(filepath_res.body, "%c", "")
   rfs.dlog("download > got filepath: <" .. filepath .. ">")

   -- check if file path is ok
   --
   if not string.match(filepath, "^/data") then
      rfs.warn("download > filepath has incorrect format")
      rfs.escape(500, "download", "stored filepath has incorrect format")
   end


   rfs.dlog("Proceeding to location: /storage" .. "-l" .. threads_limit)

   ngx.exec("/storage" .. "-l" .. threads_limit .. filepath)
end